//
//  PortfolioViewController.m
//  Stock Trading Game
//
//  Created by Clement Lau on 5/3/14.
//  Copyright (c) 2014 Clement Lau. All rights reserved.
//

#import "PortfolioViewController.h"
#import "StockTableViewCell.h"
#import "CloudMiddleman.h"
#import "StockViewController.h"
#import "CSVParser.h"

@interface PortfolioViewController ()

@end

@implementation PortfolioViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _portfolio = [NSArray array];
    self.navigationItem.title = @"Portfolio";
}

- (void)viewDidAppear:(BOOL)animated
{
    [CloudMiddleman getPortfolioFromViewController:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)portfolioCallback:(NSArray *)portfolio
{
    _portfolio = portfolio;
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ( section == 0 ) {
        return 1;
    }
    return [_portfolio count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    StockTableViewCell *cell = nil;
    if ( indexPath.section == 0 ) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"summary" forIndexPath:indexPath];
        if ( cell == nil ) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"StockTableCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        cell.symbolLabel.text = @"Summary";
        cell.descriptionLabel.text = @"";
    } else {
        cell = [tableView dequeueReusableCellWithIdentifier:@"stock" forIndexPath:indexPath];
        
        NSString *symbol = _portfolio[indexPath.row][@"Symbol"];
        NSString *changePercent = [CSVParser getChangePercentForSymbol:symbol];
        NSString *sharesOwned = [NSString stringWithFormat:@"%@ shares", _portfolio[indexPath.row][@"Shares"]];
        
        cell.symbolLabel.text = symbol;
        cell.additionalInfoLabel.text = sharesOwned;
        
        if ( [changePercent characterAtIndex:0] == '-' ) {
            changePercent = [@"⬇︎" stringByAppendingString:[changePercent substringFromIndex:1]];
            [cell.descriptionLabel setTextColor:[UIColor redColor]];
        } else {
            changePercent = [@"⬆︎" stringByAppendingString:[changePercent substringFromIndex:1]];
            [cell.descriptionLabel setTextColor:[UIColor greenColor]];
        }
        cell.descriptionLabel.text = changePercent;
        
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}




#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ( [segue.destinationViewController isMemberOfClass:[StockViewController class]] ) {
        StockViewController *vc = segue.destinationViewController;
        vc.symbol = [[sender symbolLabel] text];
    }
}


@end
