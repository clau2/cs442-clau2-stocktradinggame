//
//  ContainerViewController.m
//  Stock Trading Game
//
//  Created by Clement Lau on 4/2/14.
//  Copyright (c) 2014 Clement Lau. All rights reserved.
//

#import "ContainerViewController.h"
#import "BannerViewController.h"

#define BANNERHEIGHT 50

@interface ContainerViewController ()

@end

@implementation ContainerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
