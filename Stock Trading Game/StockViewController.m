//
//  StockViewController.m
//  Stock Trading Game
//
//  Created by Clement Lau on 4/5/14.
//  Copyright (c) 2014 Clement Lau. All rights reserved.
//

#import "StockViewController.h"
#import "CSVParser.h"
#import "TradeViewController.h"


@interface StockViewController ()

@end

@implementation StockViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationItem setTitle:self.symbol];
    _info = [CSVParser fetchDataForSymbol:[self symbol]];
    self.nameLabel.text = _info[@"name"];
    self.lastTradeLabel.text = _info[@"lastTrade"];
    self.changeLabel.text = _info[@"change"];
    self.changePercentLabel.text = _info[@"changePercent"];
    self.openLabel.text = _info[@"open"];
    
    if ( [self.changeLabel.text characterAtIndex:0] == '-' ) {
        [self.changeLabel setText:[NSString stringWithFormat:@"⬇︎%@", [self.changeLabel.text substringFromIndex:1]]];
        [self.changeLabel setTextColor:[UIColor redColor]];
        [self.changePercentLabel setText:[self.changePercentLabel.text substringFromIndex:1]];
        [self.changePercentLabel setTextColor:[UIColor redColor]];
    } else {
        [self.changeLabel setText:[NSString stringWithFormat:@"⬆︎%@", [self.changeLabel.text substringFromIndex:1]]];
        [self.changeLabel setTextColor:[UIColor greenColor]];
        [self.changePercentLabel setText:[self.changePercentLabel.text substringFromIndex:1]];
        [self.changePercentLabel setTextColor:[UIColor greenColor]];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    TradeViewController *viewController = segue.destinationViewController;
    viewController.info = _info;
    viewController.symbol = _symbol;
}

@end
