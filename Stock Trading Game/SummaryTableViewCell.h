//
//  SummaryTableViewCell.h
//  Stock Trading Game
//
//  Created by Clement Lau on 5/4/14.
//  Copyright (c) 2014 Clement Lau. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SummaryTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *label1;
@property (weak, nonatomic) IBOutlet UILabel *label2;

@end
