//
//  ConfirmTradeViewController.m
//  Stock Trading Game
//
//  Created by Clement Lau on 5/2/14.
//  Copyright (c) 2014 Clement Lau. All rights reserved.
//

#import "ConfirmTradeViewController.h"

#import "CloudMiddleman.h"

@interface ConfirmTradeViewController ()

@end

@implementation ConfirmTradeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if ( [_tradeDetails[@"Buy"] isEqualToValue:@YES] ) {
        _titleLabel.text = @"You are buying";
    } else {
        _titleLabel.text = @"You are selling";
    }
    
    _symbolLabel.text = _tradeDetails[@"Symbol"];
    
    _sharesLabel.text = [NSString stringWithFormat:@"%@",_tradeDetails[@"NumberOfShares"]];
    
    _priceLabel.text = [NSString stringWithFormat:@"% .2f", [_tradeDetails[@"TotalPrice"] doubleValue]];
    _message.text = @"";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)confirmPressed:(id)sender {
    if ( [_tradeDetails[@"Buy"] isEqualToValue:@YES] ) {
        [CloudMiddleman controller:self marketOrderBuySymbol:_tradeDetails[@"Symbol"] quantity:[_tradeDetails[@"NumberOfShares"] integerValue] price:[_tradeDetails[@"TotalPrice"] doubleValue]];
    } else {
        [CloudMiddleman controller:self marketOrderSellSymbol:_tradeDetails[@"Symbol"] quantity:[_tradeDetails[@"NumberOfShares"] integerValue] price:[_tradeDetails[@"TotalPrice"] doubleValue]];
    }
}

- (void)cannotTrade:(NSString *)message
{
    _message.text = message;
    [_confirmButton setHidden:YES];
    [_cancelButton setTitle:@"Go back" forState:UIControlStateNormal];
    [_cancelButton sizeToFit];
}

- (void)finishedTrading
{
    _message.text = @"Transaction complete";
    [_confirmButton setHidden:YES];
    [_cancelButton setTitle:@"Go back" forState:UIControlStateNormal];
    [_cancelButton sizeToFit];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
