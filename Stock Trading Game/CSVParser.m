//
//  CSVParser.m
//  Stock Trading Game
//
//  Created by Clement Lau on 4/8/14.
//  Copyright (c) 2014 Clement Lau. All rights reserved.
//

#import "CSVParser.h"

#define DOW_DIVISOR 0.15571590501117

@implementation CSVParser

+ (NSString*)getDowJones
{
    NSArray *components = @[
                            @"MMM", @"AXP", @"T", @"BA", @"CAT", @"CVX", @"CSCO",
                            @"KO", @"DD", @"XOM", @"GE", @"GS", @"HD", @"INTC",
                            @"IBM", @"JNJ", @"JPM", @"MCD", @"MRK", @"MSFT",
                            @"NKE", @"PFE", @"PG", @"TRV", @"UNH", @"UTX", @"VZ",
                            @"V", @"WMT", @"DIS"
                            ];
    
    // make url with all dow components
    NSString *url = @"http://finance.yahoo.com/d/quotes.csv?s=";
    url = [url stringByAppendingString:components[0]];
    for ( NSInteger i = 1; i < [components count]; i++ ) {
        url = [url stringByAppendingString:@","];
        url = [url stringByAppendingString:[components objectAtIndex:i]];
    }
    url = [url stringByAppendingString:@"&f=sl1p"];
    url = [url stringByReplacingOccurrencesOfString:@"^" withString:@"%5E"];
    
    NSData *a = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
    NSString *csvfile = [[NSString alloc] initWithUTF8String:[a bytes]];
    
    // separate the csv by line and remove the last line
    NSMutableArray *lines = [NSMutableArray arrayWithArray:[csvfile componentsSeparatedByString:@"\n"]];
    [lines removeLastObject];
    
    // add all the numbers together
    double lastTrade = 0;
    double prevClose = 0;
    for ( NSString *i in lines ) {
        NSArray *info = [self splitString:i];
        lastTrade += [info[1] doubleValue];
        prevClose += [info[2] doubleValue];
    }
    
    // do math
    double percentChange = (lastTrade - prevClose)/prevClose;
    percentChange *= 100;
    
    return [NSString stringWithFormat:@"% .2f%%", percentChange];
}

+ (NSArray*) splitString:(NSString*)string
{
    // this regex matches commas that are not inside quotes
    NSString *pattern = @",(?=([^\\\"]*\\\"[^\\\"]*\\\")*[^\\\"]*$)";
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:0 error:nil];
    NSRange range = NSMakeRange(0, string.length);
    NSString *split = [regex stringByReplacingMatchesInString:string options:0 range:range withTemplate:@"\n"];
    NSArray *arr = [split componentsSeparatedByString:@"\n"];
    return arr;
}

+ (NSDictionary*)fetchDataForSymbol:(NSString *)symbol
{
    // make url
    NSString *url = [NSString stringWithFormat:@"%@%@%@",
                     @"http://finance.yahoo.com/d/quotes.csv?s=",
                     symbol,
                     @"&f=snl1c1p2o"
                     ];
    url = [url stringByReplacingOccurrencesOfString:@"^" withString:@"%5E"];
    
    NSData *a = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
    NSString *csvfile = [[NSString alloc] initWithUTF8String:[a bytes]];
    
    // get the first line and split it
    NSString *line = [csvfile componentsSeparatedByString:@"\n"][0];
    NSArray *info = [self splitString:line];
    
    // parse the data
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    dict[@"symbol"]        = [info[0] stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    dict[@"name"]          = [info[1] stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    dict[@"lastTrade"]     = info[2];
    dict[@"change"]        = info[3];
    dict[@"changePercent"] = [info[4] stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    dict[@"open"]          = info[5];
    
    NSDictionary *finalDict = [NSDictionary dictionaryWithDictionary:dict];
    return finalDict;
}

+ (NSDictionary*)fetchDataForSymbols:(NSArray *)symbols
{
    bool dow = NO;
    
    // make the url
    NSString *url = @"http://finance.yahoo.com/d/quotes.csv?s=";
    url = [url stringByAppendingString:symbols[0]];
    for ( NSInteger i = 1; i < [symbols count]; i++ ) {
        url = [url stringByAppendingString:@","];
        url = [url stringByAppendingString:[symbols objectAtIndex:i]];
    }
    url = [url stringByAppendingString:@"&f=snl1c1p2o"];
    url = [url stringByReplacingOccurrencesOfString:@"^" withString:@"%5E"];
    
    NSData *a = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
    NSString *csvfile = [[NSString alloc] initWithUTF8String:[a bytes]];
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    NSMutableArray *lines = [NSMutableArray arrayWithArray:[csvfile componentsSeparatedByString:@"\n"]];
    [lines removeLastObject];
    
    for ( NSString *i in lines ) {
        NSArray *info = [self splitString:i];
        if ( [info[0] isEqualToString:@"\"INDU\""] || [info[0] isEqualToString:@"\"^DJI\""] ) {
            dow = YES;
            continue;
        }
        NSMutableDictionary *d = [NSMutableDictionary dictionary];
        d[@"name"]          = [info[1] stringByReplacingOccurrencesOfString:@"\"" withString:@""];
        d[@"lastTrade"]     = info[2];
        d[@"change"]        = info[3];
        d[@"changePercent"] = [info[4] stringByReplacingOccurrencesOfString:@"\"" withString:@""];
        d[@"open"]          = info[5];
        NSDictionary *finalDict = [NSDictionary dictionaryWithDictionary:d];
        dict[[info[0] stringByReplacingOccurrencesOfString:@"\"" withString:@""]] = finalDict;
    }
    if ( dow ) {
        NSMutableDictionary *d = [NSMutableDictionary dictionary];
        [d setObject:[self getDowJones] forKey:@"changePercent"];
        NSDictionary *finalDict = [NSDictionary dictionaryWithDictionary:d];
        [dict setObject:finalDict forKey:@"INDU"];
    }
    NSDictionary *finalDict = [NSDictionary dictionaryWithDictionary:dict];
    return finalDict;
}

+ (NSString*)getChangePercentForSymbol:(NSString *)symbol
{
    // dow jones index is a separate case
    if ( [symbol isEqualToString:@"INDU"] ) {
        return [self getDowJones];
    } else {
        
        // make url
        NSString *url = [NSString stringWithFormat:@"%@%@%@",
                         @"http://finance.yahoo.com/d/quotes.csv?s=",
                         symbol,
                         @"&f=p2"];
        url = [url stringByReplacingOccurrencesOfString:@"^" withString:@"%5E"];
        
        NSData *a = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
        NSString *csvFile = [[NSString alloc] initWithUTF8String:[a bytes]];
        
        //remove quotes
        NSString *changePercent = [csvFile stringByReplacingOccurrencesOfString:@"\"" withString:@""];
        
        return changePercent;
    }
}

@end
