//
//  CloudMiddleman.h
//  Stock Trading Game
//
//  Created by Clement Lau on 4/21/14.
//  Copyright (c) 2014 Clement Lau. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "StockSearchViewController.h"

#import "ConfirmTradeViewController.h"

#import "PortfolioViewController.h"

@interface CloudMiddleman : NSObject

+ (void)updateTableView:(UITableViewController*)viewController withSearch:(NSString*)text;
+ (void)getPortfolioFromViewController:(PortfolioViewController*)vc;
+ (void)getFavoritesWithTarget:(id)sender selector:(SEL)selector;
+ (void)getOwnedWithTarget:(id)sender selector:(SEL)selector;
+ (void)getTransactionFeeWithBlock:(void(^)(double transactionFee))block;
+ (void)getPortfolioForSymbol:(NSString*)symbol withBlock:(void(^)(NSDictionary *portfolio))block;
+ (void)getCashWithBlock:(void(^)(double cash))block;
+ (void) controller:(ConfirmTradeViewController*)vc marketOrderBuySymbol:(NSString*)symbol quantity:(NSInteger)quantity price:(double)price;
+ (void) controller:(ConfirmTradeViewController*)vc marketOrderSellSymbol:(NSString*)symbol quantity:(NSInteger)quantity price:(double)price;
+ (void) getSummaryWithBlock:(void(^)(NSArray *summary))block;
+ (void) changeFavoritesAdd:(NSArray*)add remove:(NSArray*)remove;
+ (void)getFavoritesWithChangePercentWithTarget:(id)target andSelector:(SEL)selector;

@end
