//
//  StockTableViewCell.m
//  Stock Trading Game
//
//  Created by Clement Lau on 4/3/14.
//  Copyright (c) 2014 Clement Lau. All rights reserved.
//

#import "StockTableViewCell.h"

@implementation StockTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
