//
//  CSVParser.h
//  Stock Trading Game
//
//  Created by Clement Lau on 4/8/14.
//  Copyright (c) 2014 Clement Lau. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CSVParser : NSObject

+ (NSDictionary*)fetchDataForSymbols:(NSArray*)symbols;
+ (NSDictionary*)fetchDataForSymbol:(NSString*)symbol;
+ (NSString*)getDowJones;
+ (NSString*)getChangePercentForSymbol:(NSString*)symbol;

@end
