//
//  StockSearchViewController.m
//  Stock Trading Game
//
//  Created by Clement Lau on 4/2/14.
//  Copyright (c) 2014 Clement Lau. All rights reserved.
//

#import "StockSearchViewController.h"

#import "CloudMiddleman.h"


@interface StockSearchViewController ()

@end

@implementation StockSearchViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _results = [NSArray array];
    
    [self.searchDisplayController.searchBar setPlaceholder:@"Search all stocks"];
}

- (void)viewDidAppear:(BOOL)animated
{
    _favorites = [NSMutableArray array];
    [CloudMiddleman getFavoritesWithTarget:self selector:@selector(addFavorite:)];
    
    _owned = [NSMutableArray array];
    [CloudMiddleman getOwnedWithTarget:self selector:@selector(addOwned:)];
    
    [UITableView cancelPreviousPerformRequestsWithTarget:self.tableView selector:@selector(reloadData) object:nil];
    [self.tableView performSelector:@selector(reloadData) withObject:nil afterDelay:.4];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)addFavorite:(NSDictionary *)favorite
{
    [_favorites addObject:favorite];
    [UITableView cancelPreviousPerformRequestsWithTarget:self.tableView selector:@selector(reloadData) object:nil];
    [self.tableView performSelector:@selector(reloadData) withObject:nil afterDelay:.4];
}

- (void)addOwned:(NSDictionary *)owned
{
    [_owned addObject:owned];
    [UITableView cancelPreviousPerformRequestsWithTarget:self.tableView selector:@selector(reloadData) object:nil];
    [self.tableView performSelector:@selector(reloadData) withObject:nil afterDelay:.4];
}

- (NSString*) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if ( [tableView isEqual:self.searchDisplayController.searchResultsTableView] ) {
        return nil;
    }
    if ( section == 0 ) {
        return @"Favorites";
    }
    return @"Owned";
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    if ( [tableView isEqual:self.searchDisplayController.searchResultsTableView] ) {
        return 1;
    }
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if ( [tableView isEqual:self.searchDisplayController.searchResultsTableView] ) {
        return [_results count];
    }
    if ( section == 0 ) {
        return [_favorites count];
    }
    return [_owned count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"StockCell";
    StockTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if ( cell == nil ) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"StockTableCell" owner:self options:nil];
        cell = nib[0];
    }
    
    if ( [tableView isEqual:self.searchDisplayController.searchResultsTableView] ) {
        [cell.symbolLabel setText:_results[[indexPath row]][@"Symbol"]];
        [cell.descriptionLabel setText:_results[[indexPath row]][@"Description"]];
    } else if ( [indexPath section] == 0 ) {
        [cell.symbolLabel setText:_favorites[[indexPath row]][@"Symbol"]];
        [cell.descriptionLabel setText:_favorites[[indexPath row]][@"Description"]];
    } else {
        [cell.symbolLabel setText:_owned[[indexPath row]][@"Symbol"]];
        [cell.descriptionLabel setText:_owned[[indexPath row]][@"Description"]];
    }

    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if ( [tableView isEqual:self.searchDisplayController.searchResultsTableView] ){
        [self performSegueWithIdentifier:@"StockViewSegue" sender:tableView.visibleCells[[indexPath row]]];
    }
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [StockSearchViewController cancelPreviousPerformRequestsWithTarget:self selector:@selector(queryResults) object:nil];
    [self performSelector:@selector(queryResults) withObject:nil afterDelay:1];
    
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    return YES;
}

- (void)queryResults
{
    NSString *searchText = self.searchDisplayController.searchBar.text;
    [CloudMiddleman updateTableView:self withSearch:searchText];
}

- (void)updateTableWithResults:(NSArray*)results
{
    _results = results;
    [[self.searchDisplayController searchResultsTableView] reloadData];
}


#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    StockViewController *dest = segue.destinationViewController;
    dest.symbol = [[sender symbolLabel] text];
}



@end
