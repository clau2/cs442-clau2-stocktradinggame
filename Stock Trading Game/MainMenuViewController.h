//
//  MainMenuViewController.h
//  Stock Trading Game
//
//  Created by Clement Lau on 4/2/14.
//  Copyright (c) 2014 Clement Lau. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface MainMenuViewController : UITableViewController <PFLogInViewControllerDelegate,PFSignUpViewControllerDelegate>
- (IBAction)logOutPressed:(id)sender;

@end
