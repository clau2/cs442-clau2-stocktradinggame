//
//  CloudMiddleman.m
//  Stock Trading Game
//
//  Created by Clement Lau on 4/21/14.
//  Copyright (c) 2014 Clement Lau. All rights reserved.
//

#import "CloudMiddleman.h"
#import <Parse/Parse.h>
#import "CSVParser.h"


@implementation CloudMiddleman

+ (void)updateTableView:(UITableViewController*)viewController withSearch:(NSString *)text
{
    [PFCloud callFunctionInBackground:@"search" withParameters:@{@"searchTerm" : text} block:^(id object, NSError *error) {
        NSArray* results = object;
        NSMutableArray* results1 = [NSMutableArray array];
        for ( PFObject* result in results ) {
            [results1 addObject:@{
                                  @"Symbol" : result[@"Symbol"],
                                  @"Description" : result[@"Description"]
                                  }];
        }
        [viewController performSelector:@selector(updateTableWithResults:) withObject:[NSArray arrayWithArray:results1]];
    }];
}

+ (void)getPortfolioFromViewController:(PortfolioViewController*) vc
{
    PFQuery *portfolioQuery = [PFQuery queryWithClassName:@"Portfolio"];
    [portfolioQuery whereKey:@"userID" equalTo:[PFUser currentUser]];
    [portfolioQuery getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        [vc performSelector:@selector(portfolioCallback:) withObject:object[@"stocks"]];
    }];
}

+ (void)getFavoritesWithTarget:(id)sender selector:(SEL)selector
{
    PFQuery *favoritesQuery = [PFQuery queryWithClassName:@"UserData"];
    [favoritesQuery whereKey:@"userID" equalTo:[PFUser currentUser]];
    [favoritesQuery getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        NSMutableArray *favorites = object[@"favorites"];
        [favorites removeObject:@"INDU"];
        [favorites removeObject:@"^IXIC"];
        [favorites removeObject:@"^GSPC"];
        for ( NSString *fave in favorites ) {
            PFQuery *stocksQuery = [PFQuery queryWithClassName:@"Stocks"];
            [stocksQuery whereKey:@"Symbol" equalTo:fave];
            [stocksQuery getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
                [sender performSelector:selector withObject:@{
                                 @"Symbol" : fave,
                                 @"Description" : object[@"Description"]
                                 }];
            }];
        }
    }];
}

+ (void)getOwnedWithTarget:(id)sender selector:(SEL)selector
{
    PFQuery *ownedQuery = [PFQuery queryWithClassName:@"Portfolio"];
    [ownedQuery whereKey:@"userID" equalTo:[PFUser currentUser]];
    [ownedQuery getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        NSArray *owned = object[@"stocks"];
        for ( NSDictionary *stock in owned ) {
            PFQuery *stocksQuery = [PFQuery queryWithClassName:@"Stocks"];
            [stocksQuery whereKey:@"Symbol" equalTo:stock[@"Symbol"]];
            [stocksQuery getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
                [sender performSelector:selector withObject:@{
                                 @"Symbol" : stock[@"Symbol"],
                                 @"Description" : object[@"Description"]
                                 }];
            }];
        }
    }];
}

+ (void)getTransactionFeeWithBlock:(void(^)(double transactionFee))block
{
    PFQuery *constantsQuery = [PFQuery queryWithClassName:@"GameConstants"];
    [constantsQuery getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        block([object[@"transactionFee"] doubleValue]);
    }];
}

+ (void)getPortfolioForSymbol:(NSString *)symbol withBlock:(void(^)(NSDictionary *portfolio))block
{
    PFQuery *portfolioQuery = [PFQuery queryWithClassName:@"Portfolio"];
    [portfolioQuery whereKey:@"userID" equalTo:[PFUser currentUser]];
    [portfolioQuery getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        NSArray *stocks = object[@"stocks"];
        for ( NSDictionary *dict in stocks ) {
            if ( [dict[@"Symbol"] isEqualToString:symbol] ) {
                block(dict);
                return;
            }
        }
        block(@{
                @"Symbol": symbol,
                @"Shares": @0
                });
    }];
}

+ (void)getCashWithBlock:(void(^)(double cash))block
{
    PFQuery *portfolioQuery = [PFQuery queryWithClassName:@"Portfolio"];
    [portfolioQuery whereKey:@"userID" equalTo:[PFUser currentUser]];
    [portfolioQuery getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        block([object[@"wallet"] doubleValue]);
    }];
}

+ (void) controller:(ConfirmTradeViewController*)vc marketOrderBuySymbol:(NSString*)symbol quantity:(NSInteger)quantity price:(double)price
{
    PFQuery *portfolioQuery = [PFQuery queryWithClassName:@"Portfolio"];
    [portfolioQuery whereKey:@"userID" equalTo:[PFUser currentUser]];
    [portfolioQuery getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        if ( [object[@"wallet"] doubleValue] < price ) {
            [vc performSelector:@selector(cannotTrade:) withObject:@"Not enough cash"];
            return;
        }
        object[@"wallet"] = [NSNumber numberWithDouble:[object[@"wallet"] doubleValue] - price];
        NSMutableArray *stocks = object[@"stocks"];
        bool found = NO;
        for ( NSMutableDictionary *stock in stocks ) {
            if ( [stock[@"Symbol"] isEqualToString:symbol] ) {
                stock[@"Shares"] = [NSNumber numberWithInteger:[stock[@"Shares"] integerValue] + quantity];
                found = YES;
                break;
            }
        }
        if ( !found ) {
            [stocks addObject:@{
                               @"Symbol": symbol,
                               @"Shares": [NSNumber numberWithInteger:quantity]
                               }];
        }
        object[@"stocks"] = stocks;
        [object saveInBackgroundWithTarget:vc selector:@selector(finishedTrading)];
    }];
}

+ (void) controller:(ConfirmTradeViewController*)vc marketOrderSellSymbol:(NSString*)symbol quantity:(NSInteger)quantity price:(double)price
{
    PFQuery *portfolioQuery = [PFQuery queryWithClassName:@"Portfolio"];
    [portfolioQuery whereKey:@"userID" equalTo:[PFUser currentUser]];
    [portfolioQuery getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        NSMutableArray *stocks = object[@"stocks"];
        bool found = NO;
        for ( NSMutableDictionary *stock in stocks ) {
            if ( [stock[@"Symbol"] isEqualToString:symbol] ) {
                if ( [stock[@"Shares"] integerValue] < quantity ) {
                    [vc performSelector:@selector(cannotTrade:) withObject:@"You do not own enough shares of this stock."];
                    return;
                }
                stock[@"Shares"] = [NSNumber numberWithInteger:[stock[@"Shares"] integerValue] - quantity];
                object[@"wallet"] = [NSNumber numberWithDouble:[object[@"wallet"] doubleValue] + price];
                found = YES;
                NSLog(@"%@", stock[@"Shares"]);
                if ( [stock[@"Shares"] integerValue] == 0 ) {
                    NSLog(@"removing!!");
                    [stocks removeObject:stock];
                }
                break;
            }
        }
        if ( !found ) {
            [vc performSelector:@selector(cannotTrade:) withObject:@"You do not own shares of this stock."];
            return;
        }
        object[@"stocks"] = stocks;
        [object saveInBackgroundWithTarget:vc selector:@selector(finishedTrading)];
    }];
}

+ (void)getSummaryWithBlock:(void (^)(NSArray *))block
{
    PFQuery *portfolioQuery = [PFQuery queryWithClassName:@"Portfolio"];
    [portfolioQuery whereKey:@"userID" equalTo:[PFUser currentUser]];
    [portfolioQuery getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        NSMutableArray *s = [NSMutableArray array];
        
        [s addObject:@[@"Cash",[NSString stringWithFormat:@"$%.2f", [object[@"wallet"] doubleValue]]]];
        
        double netWorth = 0;
        for ( NSDictionary *stock in object[@"stocks"] ) {
            NSDictionary *stockInfo = [CSVParser fetchDataForSymbol:stock[@"Symbol"]];
            netWorth += [stock[@"Shares"] doubleValue] * [stockInfo[@"lastTrade"] doubleValue];
        }
        [s addObject:@[@"Net worth",[NSString stringWithFormat:@"$%.2f", netWorth]]];
        
        block([NSArray arrayWithArray:s]);
    }];
}

+ (void)changeFavoritesAdd:(NSArray *)add remove:(NSArray *)remove
{
    PFQuery *userDataQuery = [PFQuery queryWithClassName:@"UserData"];
    [userDataQuery whereKey:@"userID" equalTo:[PFUser currentUser]];
    [userDataQuery getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        NSMutableArray *favorites = object[@"favorites"];
        for ( NSString *stockToAdd in add ) {
            if ( ![favorites containsObject:stockToAdd] ) {
                [favorites addObject:stockToAdd];
            }
        }
        for ( NSString *stockToRemove in remove ) {
            [favorites removeObject:stockToRemove];
        }
        object[@"favorites"] = favorites;
        [object saveEventually];
    }];
}

+ (void)getFavoritesWithChangePercentWithTarget:(id)target andSelector:(SEL)selector
{
    PFQuery *userDataQuery = [PFQuery queryWithClassName:@"UserData"];
    [userDataQuery whereKey:@"userID" equalTo:[PFUser currentUser]];
    [userDataQuery getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        NSArray *favorites = object[@"favorites"];
        NSMutableArray *favoritesWithChangePercent = [NSMutableArray array];
        for ( NSString *fave in favorites ) {
            NSString *changePercent = [CSVParser getChangePercentForSymbol:fave];
            if ( [changePercent characterAtIndex:0] == '-' ) {
                changePercent = [@"⬇︎" stringByAppendingString:[changePercent substringFromIndex:1]];;
            } else {
                changePercent = [@"⬆︎" stringByAppendingString:[changePercent substringFromIndex:1]];
            }
            [favoritesWithChangePercent
             addObject:@{
                         @"Symbol": fave,
                         @"ChangePercent": changePercent
                         }];
        }
        [target performSelector:selector withObject:[NSArray arrayWithArray:favoritesWithChangePercent]];
    }];
}

@end
