//
//  StockSearchViewController.h
//  Stock Trading Game
//
//  Created by Clement Lau on 4/2/14.
//  Copyright (c) 2014 Clement Lau. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StockTableViewCell.h"
#import "StockViewController.h"

@interface StockSearchViewController : UITableViewController <UISearchDisplayDelegate, UISearchBarDelegate>

@property (strong, nonatomic) IBOutlet UITableView *stockTableView;
@property (strong) NSArray* results;
@property (strong) NSMutableArray* favorites;
@property (strong) NSMutableArray* owned;

- (void)queryResults;
- (void)updateTableWithResults:(NSArray*)results;
- (void)addFavorite:(NSDictionary*)favorite;
- (void)addOwned:(NSDictionary*)owned;

@end
