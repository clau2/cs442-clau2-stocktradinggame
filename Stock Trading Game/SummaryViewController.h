//
//  SummaryViewController.h
//  Stock Trading Game
//
//  Created by Clement Lau on 5/4/14.
//  Copyright (c) 2014 Clement Lau. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SummaryViewController : UITableViewController
@property (strong) NSArray *summary;

@end
