//
//  StockViewController.h
//  Stock Trading Game
//
//  Created by Clement Lau on 4/5/14.
//  Copyright (c) 2014 Clement Lau. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StockViewController : UIViewController

@property (strong) NSString *symbol;
//@property (weak, nonatomic) IBOutlet UILabel *symbolLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *lastTradeLabel;
@property (weak, nonatomic) IBOutlet UILabel *changeLabel;
@property (weak, nonatomic) IBOutlet UILabel *changePercentLabel;
@property (weak, nonatomic) IBOutlet UILabel *openLabel;
@property (strong) NSDictionary *info;

@end
