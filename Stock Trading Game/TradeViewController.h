//
//  TradeViewController.h
//  Stock Trading Game
//
//  Created by Clement Lau on 4/7/14.
//  Copyright (c) 2014 Clement Lau. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TradeViewController : UIViewController <UITextFieldDelegate>
- (IBAction)valueChanged:(id)sender;
@property (weak, nonatomic) IBOutlet UISegmentedControl *buySellSelector;
@property (assign) BOOL order;
@property (assign) BOOL buy;
@property (weak, nonatomic) IBOutlet UILabel *sharesOwnedLabel;
@property (weak, nonatomic) IBOutlet UILabel *cashLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderLabel;
- (IBAction)orderSwitchChanged:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *sharesTextField;
- (IBAction)textFieldEdittingChanged:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *priceTextField;
@property (weak, nonatomic) IBOutlet UILabel *subtotalLabel;
@property (weak, nonatomic) IBOutlet UILabel *transactionFeeLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalLabel;
@property (weak, nonatomic) IBOutlet UIButton *buySellButton;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (strong) NSString *symbol;
@property (strong) NSDictionary *info;
@property (strong) NSNumber* sharesOwned;
@property (assign) double transactionFee;

@end
