//
//  MainMenuViewController.m
//  Stock Trading Game
//
//  Created by Clement Lau on 4/2/14.
//  Copyright (c) 2014 Clement Lau. All rights reserved.
//

#import "MainMenuViewController.h"
#import <Parse/Parse.h>
#import "CSVParser.h"

@interface MainMenuViewController ()

@end

@implementation MainMenuViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) test
{
//    [CSVParser getDowJones];
//    NSString *testString = @"\"NKE\",\"Nike, Inc. Common\",72.70,-0.69,\"-0.94%\",73.09";
//    NSArray *arr = [CSVParser splitString:testString];
//    for ( NSString *str in arr ) {
//        NSLog(@"%@",str);
//    }
//    [PFCloud callFunctionInBackground:@"search" withParameters:@{@"searchTerm" : @"yahoo!"} block:^(id object, NSError *error) {
//        for ( PFObject *o in object ) {
//            NSLog(@"%@: %@", o[@"Symbol"], o[@"Description"]);
//        }
//    }];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self test];
    if ( [PFUser currentUser] == nil ) {
        PFLogInViewController *logInController = [[PFLogInViewController alloc] init];
        logInController.delegate = self;
        logInController.fields = logInController.fields ^PFLogInFieldsDismissButton;
        logInController.signUpController.delegate = self;
        [self presentViewController:logInController animated:NO completion:NULL];
    } else {
        [[self navigationItem] setTitle:[@"Welcome, " stringByAppendingString:[[PFUser currentUser] username]]];
    }
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)signUpViewController:(PFSignUpViewController *)signUpController didSignUpUser:(PFUser *)user
{
    [PFCloud callFunctionInBackground:@"newUser" withParameters:@{} block:NULL];
    [signUpController dismissViewControllerAnimated:YES completion:NULL];
}

- (void)logInViewController:(PFLogInViewController *)logInController didLogInUser:(PFUser *)user
{
    
    [[self navigationItem] setTitle:[@"Welcome, " stringByAppendingString:[[PFUser currentUser] username]]];
    [logInController dismissViewControllerAnimated:YES completion:NULL];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 3;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


- (IBAction)logOutPressed:(id)sender {
    [PFUser logOut];
    PFLogInViewController *logInController = [[PFLogInViewController alloc] init];
    logInController.delegate = self;
    logInController.fields = logInController.fields ^PFLogInFieldsDismissButton;
    logInController.signUpController.delegate = self;
    [self presentViewController:logInController animated:YES completion:NULL];
}
@end
