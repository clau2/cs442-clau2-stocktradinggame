//
//  TradeViewController.m
//  Stock Trading Game
//
//  Created by Clement Lau on 4/7/14.
//  Copyright (c) 2014 Clement Lau. All rights reserved.
//

#import "TradeViewController.h"

#import "CloudMiddleman.h"

#import "ConfirmTradeViewController.h"

@interface TradeViewController ()

@end

@implementation TradeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _sharesOwned = @0;
    
    _messageLabel.text = @"";
    _messageLabel.adjustsFontSizeToFitWidth = @YES;
    
    [self.navigationItem setTitle:@"Trade"];
    _order = YES; // YES is market order, NO is limit order
    
    _priceTextField.text = [NSString stringWithFormat:@"% .2f", [_info[@"lastTrade"] doubleValue]];
    
    [CloudMiddleman getTransactionFeeWithBlock:^(double transactionFee) {
        _transactionFee = transactionFee;
        _transactionFeeLabel.text = [NSString stringWithFormat:@"% .2f",_transactionFee];
        [self updateSubtotal];
        [_transactionFeeLabel sizeToFit];
        _subtotalLabel.adjustsFontSizeToFitWidth = YES;
    }];
    
    if ( _buySellSelector.selected == 1 ) {
        [_buySellButton setTitle:@"Sell" forState:UIControlStateNormal];
        _buy = NO;
    } else {
        [_buySellButton setTitle:@"Buy" forState:UIControlStateNormal];
        _buy = YES;
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [CloudMiddleman getPortfolioForSymbol:_info[@"symbol"] withBlock:^(NSDictionary *portfolio) {
        _sharesOwned = portfolio[@"Shares"];
        [_sharesOwnedLabel setText:[NSString stringWithFormat:@"You own %@ shares", _sharesOwned]];
        [_sharesOwnedLabel sizeToFit];
    }];
    
    [CloudMiddleman getCashWithBlock:^(double cash) {
        _cashLabel.text = [NSString stringWithFormat:@"You have $%.2f", cash];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    ConfirmTradeViewController *vc = segue.destinationViewController;
    vc.tradeDetails = @{
                        @"Buy": (_buy ? @YES : @NO),
                        @"Symbol": _symbol,
                        @"NumberOfShares": [NSNumber numberWithInteger:[_sharesTextField.text integerValue]],
                        @"TotalPrice": [NSNumber numberWithDouble:[_totalLabel.text doubleValue]]
                        };
}


- (IBAction)valueChanged:(id)sender {
    if ( _buySellSelector.selectedSegmentIndex == 1 ) {
        [_buySellButton setTitle:@"Sell" forState:UIControlStateNormal];
        _buy = NO;
        if ( [_sharesTextField.text integerValue] > [_sharesOwned integerValue] ) {
            _sharesTextField.text = [NSString stringWithFormat:@"%@", _sharesOwned];
        }
        _transactionFeeLabel.text = @"0";
        [self updateSubtotal];
    } else {
        [_buySellButton setTitle:@"Buy" forState:UIControlStateNormal];
        _buy = YES;
        _transactionFeeLabel.text = [NSString stringWithFormat:@"% .2f",_transactionFee];
        [self updateSubtotal];
    }
}
- (IBAction)orderSwitchChanged:(id)sender {
    _order = !_order;
    if ( _order ) {
        _orderLabel.text = @"Market order";
        [_priceTextField setText:_info[@"lastTrade"]];
        [_priceTextField setUserInteractionEnabled:NO];
        [_priceTextField setBorderStyle:UITextBorderStyleNone];
    } else {
        _orderLabel.text = @"Limit order";
        [_priceTextField setUserInteractionEnabled:YES];
        [_priceTextField setBorderStyle:UITextBorderStyleRoundedRect];
    }
}

- (void)updateSubtotal
{
    NSInteger shares = [_sharesTextField.text integerValue];
    double price = [_priceTextField.text doubleValue];
    [_subtotalLabel setText:[NSString stringWithFormat:@"% .2f", shares * price]];
    [self updateTotal];
}

- (void)updateTotal
{
    double subtotal = [_subtotalLabel.text doubleValue];
    double transactionFee = [_transactionFeeLabel.text doubleValue];
    [_totalLabel setText:[NSString stringWithFormat:@"% .2f", subtotal + transactionFee]];
    [_totalLabel sizeToFit];
}

- (IBAction)textFieldEdittingChanged:(id)sender {
    [self updateSubtotal];
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    if ( [textField isEqual:_priceTextField] ) {
        // check to see if it's buy or sell, then if the price should be greater or less than
    } else {
        if ( !_buy && [textField.text integerValue] > [_sharesOwned integerValue] ) {
            _messageLabel.text = @"You don't own enough shares!";
            
            textField.text = [NSString stringWithFormat:@"%@", _sharesOwned];
            
            [self updateSubtotal];
        } else {
            _messageLabel.text = @"";
        }
    }
    return YES;
}

- (IBAction)unwindSegue:(UIStoryboardSegue*) segue
{
    
}


@end
