//
//  PortfolioViewController.h
//  Stock Trading Game
//
//  Created by Clement Lau on 5/3/14.
//  Copyright (c) 2014 Clement Lau. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PortfolioViewController : UITableViewController

- (void) portfolioCallback:(NSArray*)portfolio;

@property (strong) NSArray *portfolio;

@end
