//
//  ConfirmTradeViewController.h
//  Stock Trading Game
//
//  Created by Clement Lau on 5/2/14.
//  Copyright (c) 2014 Clement Lau. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConfirmTradeViewController : UIViewController

@property (strong) NSDictionary* tradeDetails;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *symbolLabel;
@property (weak, nonatomic) IBOutlet UILabel *sharesLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *message;
@property (weak, nonatomic) IBOutlet UIButton *confirmButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;

- (IBAction)confirmPressed:(id)sender;
- (void) cannotTrade:(NSString*)message;
- (void) finishedTrading;

@end
