//
//  BannerViewController.m
//  Stock Trading Game
//
//  Created by Clement Lau on 4/2/14.
//  Copyright (c) 2014 Clement Lau. All rights reserved.
//

#import "BannerViewController.h"
#import "CloudMiddleman.h"

#define LABEL_MARGIN 20
#define CYCLE_MARGIN 150

@interface BannerViewController ()

@end

@implementation BannerViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void) cycleLabels {
    int offset = 0;
    int widestFrame = 0;
    for ( UILabel *l in self.labels ) {
        offset += l.frame.size.width;
        widestFrame = (widestFrame > l.frame.size.width) ? widestFrame : l.frame.size.width;
    }
    offset += (self.labels.count - 1) * LABEL_MARGIN + CYCLE_MARGIN;
    offset = (offset > self.view.bounds.size.width + widestFrame + CYCLE_MARGIN) ? offset : self.view.bounds.size.width + widestFrame + CYCLE_MARGIN;
    for ( UILabel *l in self.labels ) {
        if ( l.frame.origin.x + l.frame.size.width < 0 ) {
            l.frame = CGRectOffset(l.frame, offset, 0);
        }
    }
}

// scroll moves the labels a little to the right, then cycles
// them back as necessary, and then calls itself again

- (void)scroll
{
    [UIView animateWithDuration:0.1 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        for ( UILabel *label in self.labels ) {
            label.frame = CGRectOffset(label.frame, -5, 0);
        }
    }completion:^(BOOL finished) {
        if ( finished ) {
            [self cycleLabels];
            [self performSelector:@selector(scroll) withObject:nil];
        }
    }];
}

// makeLabels: makes labels and adds them to the view

- (void)makeLabels:(NSArray*)favorites
{
    self.labels = nil;
    NSMutableArray *temp = [NSMutableArray array];
    for ( int i = 0; i < favorites.count; i++ ) {
        
        UILabel *label = [[UILabel alloc] init];
        [self.view addSubview:label];
        // create a label and add it as a subview
        
        NSString *symbol = favorites[i][@"Symbol"];
        NSString *changePercent = favorites[i][@"ChangePercent"];
        if ( [changePercent hasPrefix:@"⬆︎"] ) {
            [label setTextColor:[UIColor greenColor]];
        } else if ( [changePercent hasPrefix:@"⬇︎"] ){
            [label setTextColor:[UIColor redColor]];
        }
        // set the color of the label depending on how the stock changed
        
        if ( [symbol isEqualToString:@"INDU"] ) {
            symbol = @"Dow";
        } else if ( [symbol isEqualToString:@"^IXIC"] ) {
            symbol = @"Nasdaq";
        } else if ( [symbol isEqualToString:@"^GSPC"] ) {
            symbol = @"S&P 500";
        }
        // change the ticker symbol if it's a market index
        
        [label setText:[NSString stringWithFormat:@"%@ %@",symbol,changePercent]];
        
        int x = self.view.bounds.size.width / 3;
        if ( i > 0 ) {
            x = [temp[i-1] frame].origin.x + [temp[i-1] frame].size.width + LABEL_MARGIN;
        }
        [label setFrame:CGRectMake(x, 0, 1, 50)];
        [label sizeToFit];
        [temp addObject:label];
    }
    self.labels = [NSArray arrayWithArray:temp];
}

- (void)viewDidAppear:(BOOL)animated
{
    for ( UILabel *l in self.labels ) {
        [l removeFromSuperview];
    }
    [CloudMiddleman getFavoritesWithChangePercentWithTarget:self andSelector:@selector(makeLabels:)];
    [self scroll];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
