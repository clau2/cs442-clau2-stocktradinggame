//
//  FavoritesSelectorTableViewController.m
//  Stock Trading Game
//
//  Created by Clement Lau on 5/4/14.
//  Copyright (c) 2014 Clement Lau. All rights reserved.
//

#import "FavoritesSelectorTableViewController.h"
#import "StockTableViewCell.h"
#import "CloudMiddleman.h"

@interface FavoritesSelectorTableViewController ()

@end

@implementation FavoritesSelectorTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _stockDescriptions = [NSMutableDictionary dictionary];
    _results = [NSArray array];
}

- (void)viewDidAppear:(BOOL)animated
{
    _stocksToAdd = [NSMutableArray array];
    
    _stocksToRemove = [NSMutableArray array];
    
    _favorites = [NSMutableArray array];
    [CloudMiddleman getFavoritesWithTarget:self selector:@selector(addFavorite:)];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [self commitChanges];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [StockSearchViewController cancelPreviousPerformRequestsWithTarget:self selector:@selector(queryResults) object:nil];
    [self performSelector:@selector(queryResults) withObject:nil afterDelay:1];
    
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    return YES;
}

- (void)queryResults
{
    NSString *searchText = self.searchDisplayController.searchBar.text;
    [CloudMiddleman updateTableView:self withSearch:searchText];
}

- (void)updateTableWithResults:(NSArray*)results
{
    _results = results;
    for ( NSDictionary *result in _results ) {
        NSString *symbol = result[@"Symbol"];
        NSString *desc = result[@"Description"];
        _stockDescriptions[symbol] = desc;
    }
    [[self.searchDisplayController searchResultsTableView] reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ( [tableView isEqual:self.tableView] ) {
        return [_favorites count];
    } else {
        return [_results count];
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"StockCell";
    StockTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if ( cell == nil ) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"StockTableCell" owner:self options:nil];
        cell = nib[0];
    }
    
    if ( [tableView isEqual:self.tableView] ) {
        NSString *symbol = _favorites[indexPath.row];
        NSString *desc = _stockDescriptions[symbol];
        cell.symbolLabel.text = symbol;
        cell.descriptionLabel.text = desc;
        if ( ![_stocksToRemove containsObject:symbol] ) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        } else {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    } else {
        NSString *symbol = _results[indexPath.row][@"Symbol"];
        NSString *desc = _results[indexPath.row][@"Description"];
        cell.symbolLabel.text = symbol;
        cell.descriptionLabel.text = desc;
        if ( ([_favorites containsObject:symbol] && ![_stocksToRemove containsObject:symbol]) || [_stocksToAdd containsObject:symbol] ) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        } else {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    StockTableViewCell *cell = (StockTableViewCell*) [tableView cellForRowAtIndexPath:indexPath];
    if ( cell.accessoryType == UITableViewCellAccessoryCheckmark ) {
        cell.accessoryType = UITableViewCellAccessoryNone;
        [self removeStockIfNeeded:cell.symbolLabel.text];
        
    } else {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        [self addStockIfNeeded:cell.symbolLabel.text];
    }
    if ( [tableView isEqual:self.searchDisplayController.searchResultsTableView] ) {
        StockTableViewCell *cell = (StockTableViewCell*) [tableView cellForRowAtIndexPath:indexPath];
        NSString *symbol = cell.symbolLabel.text;
        [_favorites addObject:symbol];
        [self.tableView reloadData];
    }
}

- (void)removeStockIfNeeded:(NSString*)stock
{
    if ( [_stocksToAdd containsObject:stock] ) {
        [_stocksToAdd removeObject:stock];
    } else if ( ![_stocksToRemove containsObject:stock] ) {
        [_stocksToRemove addObject:stock];
    }
}

- (void)addStockIfNeeded:(NSString*)stock
{
    if ( [_stocksToRemove containsObject:stock] ) {
        [_stocksToRemove removeObject:stock];
    } else if ( ![_stocksToAdd containsObject:stock] ) {
        [_stocksToAdd addObject:stock];
    }
}

- (void)commitChanges
{
    [CloudMiddleman changeFavoritesAdd:_stocksToAdd remove:_stocksToRemove];
}

- (void)addFavorite:(NSDictionary *)favoriteDict
{
    NSString *symbol = favoriteDict[@"Symbol"];
    NSString *desc = favoriteDict[@"Description"];
    [_favorites addObject:symbol];
    _stockDescriptions[symbol] = desc;
    
    [UITableView cancelPreviousPerformRequestsWithTarget:self.tableView selector:@selector(reloadData) object:nil];
    [self.tableView performSelector:@selector(reloadData) withObject:nil afterDelay:.4];
}


@end
