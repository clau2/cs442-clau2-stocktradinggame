//
//  BannerViewController.h
//  Stock Trading Game
//
//  Created by Clement Lau on 4/2/14.
//  Copyright (c) 2014 Clement Lau. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "CSVParser.h"

@interface BannerViewController : UIViewController

@property NSArray *labels;

- (void)makeLabels:(NSArray*)favorites;

@end
