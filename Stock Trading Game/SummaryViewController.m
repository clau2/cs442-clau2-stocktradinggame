//
//  SummaryViewController.m
//  Stock Trading Game
//
//  Created by Clement Lau on 5/4/14.
//  Copyright (c) 2014 Clement Lau. All rights reserved.
//

#import "SummaryViewController.h"
#import "SummaryTableViewCell.h"
#import "CloudMiddleman.h"

@interface SummaryViewController ()

@end

@implementation SummaryViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _summary = [NSArray array];
    [CloudMiddleman getSummaryWithBlock:^(NSArray *summary) {
        _summary = summary;
        [self.tableView reloadData];
    }];
    
    self.navigationItem.title = @"Summary";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _summary.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SummaryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"summaryCell" forIndexPath:indexPath];
    
    cell.label1.text = _summary[indexPath.row][0];
    cell.label2.text = _summary[indexPath.row][1];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}



@end
