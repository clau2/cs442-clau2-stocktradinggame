//
//  FavoritesSelectorTableViewController.h
//  Stock Trading Game
//
//  Created by Clement Lau on 5/4/14.
//  Copyright (c) 2014 Clement Lau. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FavoritesSelectorTableViewController : UITableViewController <UISearchDisplayDelegate>

@property (strong) NSMutableArray *favorites;
@property (strong) NSMutableDictionary *stockDescriptions;
@property (strong) NSMutableArray *stocksToAdd;
@property (strong) NSMutableArray *stocksToRemove;
@property (strong) NSArray *results;

- (void)addFavorite:(NSString*)favorite;

@end
